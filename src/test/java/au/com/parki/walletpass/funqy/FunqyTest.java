package au.com.parki.walletpass.funqy;

import au.com.parki.walletpass.funqy.dto.WalletPassRequest;
import io.quarkus.amazon.lambda.test.LambdaClient;
import org.junit.jupiter.api.Test;
import io.quarkus.test.junit.QuarkusTest;

import javax.ws.rs.core.Response;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;

@QuarkusTest
public class FunqyTest {

    @Test
    public void testFunqyLambda() throws Exception {
        // you test your lambas by invoking on http://localhost:8081
        // this works in dev mode too
        WalletPassRequest in = new WalletPassRequest();
        in.setETin("29437011720187");
        //Windows
        //in.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36");
        //Android phone
        //in.setUserAgent("Mozilla/5.0 (Linux; Android 11; SM-A715F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Mobile Safari/537.36");
        //Android tablet
        //in.setUserAgent("Mozilla/5.0 (Linux; Android 7.1.1; SM-T555 Build/NMF26X; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/83.0.4103.96 Safari/537.36");
        //Iphone
        //in.setUserAgent("Mozilla/5.0 (iPhone; CPU iPhone OS 14_6 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.1 Mobile/15E148 Safari/604.1");
        //Ipad
        //in.setUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1 Safari/605.1.15");
        //MAC
        in.setUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.55 Safari/537.36");
        //System.out.print(in.getUserAgent());
        System.out.println("input: "+in.getETin());
        Response out = LambdaClient.invoke(Response.class, in);
        System.out.println(out);
/*
        Person in = new Person();
        in.setName("Bill");
        given()
                .contentType("application/json")
                .accept("application/json")
                .body(in)
                .when()
                .post()
                .then()
                .statusCode(200)
                .body(containsString("Hello Bill"));*/
    }

}

