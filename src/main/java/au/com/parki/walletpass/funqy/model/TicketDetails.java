package au.com.parki.walletpass.funqy.model;

import org.joda.time.DateTime;

import java.math.BigDecimal;

public class TicketDetails {

    private Long id;
    private String etin;
    private String zoneName;
    private String carparkImage;
    private String registration;
    private DateTime startDate;
    private DateTime endDate;



    public static TicketDetails getInstance(Reservation reservation) {
        TicketDetails out = new TicketDetails();
        out.setId(reservation.getId());
        out.setEtin(reservation.getEtin());
        out.setZoneName(reservation.getZone().getZoneName());
        out.setCarparkImage(reservation.getZone().getCarparkImage());
        out.setRegistration(reservation.getVehicle().getRegistration());
        out.setStartDate(reservation.getStartDate());
        out.setEndDate(reservation.getEndDate());

        System.out.println(reservation.getId());
        System.out.println(reservation.getEtin());
        System.out.println(reservation.getZone().getZoneName());
        System.out.println(reservation.getZone().getCarparkImage());
        System.out.println(reservation.getVehicle().getRegistration());
        System.out.println(reservation.getStartDate());
        System.out.println(reservation.getEndDate());
        return out;
    }
/*
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = Constants.Zone.LATITUDE, column = @Column(name = "ne_latitude")),
            @AttributeOverride(name = Constants.Zone.LONGITUDE, column = @Column(name = "ne_longitude"))
    })
    private Location northEast;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = Constants.Zone.LATITUDE, column = @Column(name = "sw_latitude")),
            @AttributeOverride(name = Constants.Zone.LONGITUDE, column = @Column(name = "sw_longitude"))
    })
    private Location southWest;
*/

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public String getEtin() { return etin; }
    public void setEtin(String etin) { this.etin = etin; }

    public String getZoneName() { return zoneName; }
    public void setZoneName(String zoneName) { this.zoneName = zoneName; }

    public String getCarparkImage() { return carparkImage; }
    public void setCarparkImage(String carparkImage) { this.carparkImage = carparkImage; }

    public String getRegistration() { return registration; }
    public void setRegistration(String registration) { this.registration = registration; }

    public DateTime getStartDate() { return startDate; }
    public void setStartDate(DateTime startDate) { this.startDate = startDate; }

    public DateTime getEndDate() { return endDate; }
    public void setEndDate(DateTime endDate) { this.endDate = endDate; }
}
