package au.com.parki.walletpass.funqy.dto;

public class WalletPassRequest {

    private String eTin;
    private String userAgent;

    public String getETin() { return eTin; }
    public void setETin(String eTin) { this.eTin = eTin; }

    public String getUserAgent() { return userAgent; }
    public void setUserAgent(String userAgent) { this.userAgent = userAgent; }
}
