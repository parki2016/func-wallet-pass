package au.com.parki.walletpass.funqy.dao;

import au.com.parki.walletpass.funqy.model.Reservation;
import au.com.parki.walletpass.funqy.model.TicketDetails;
import io.quarkus.runtime.Startup;
import org.joda.time.DateTime;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import java.sql.Date;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class WalletPassDao {

    @Inject
    EntityManager em;

    private static final String SEARCH_TICKET = "SELECT r.id, r.etin, " +
            "z.zone_name AS zoneName, z.carpark_image AS carparkImage, v.registration, " +
            "CONVERT_TZ(r.start_date, 'UTC', o.timezone) AS startDate, " +
            "CONVERT_TZ(r.end_date, 'UTC', o.timezone) AS endDate " +
            //TODO: additional for location
            "FROM reservation r " +
            "INNER JOIN zone z ON r.zone_fk = z.id " +
            "INNER JOIN organisation o ON z.organisation_fk = o.id " +
            "INNER JOIN vehicle v ON r.vehicle_fk = v.id " +
            "WHERE r.etin = :etin";
    private static final String SEARCH_RESERVATION = "SELECT r FROM Reservation r " +
            "WHERE r.etin = :etin";
    private static final String ETIN = "etin";

    public TicketDetails getTicketDetails(String eTin) {
        Reservation reservationTicket = (Reservation) em.createQuery(SEARCH_RESERVATION, Reservation.class)
                .setParameter(ETIN, eTin)
                .getSingleResult();
        return TicketDetails.getInstance(reservationTicket);
        /*List<Object[]> list = em.createNativeQuery(SEARCH_TICKET)
                .setParameter(ETIN, eTin)
                .getResultList();
        if(list.size() > 0) {
            System.out.println("list size: "+list.size());
            return TicketDetails.getInstance(list.get(0));
        }*/
        //return null;
    }

}
