package au.com.parki.walletpass.funqy;

import au.com.parki.walletpass.funqy.dto.WalletPassRequest;
import au.com.parki.walletpass.funqy.service.WalletPassService;
import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;
import io.quarkus.funqy.Funq;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.transaction.Transactional;
import java.io.ByteArrayInputStream;
import java.io.IOException;

public class WalletPassFunction {

    @Inject
    WalletPassService walletPassService;

    /** The Constant PASSKIT_CONTENT_TYPE. */
    private static final String PASSKIT_CONTENT_TYPE = "application/vnd.apple.pkpass";

    /**
     * Gets the pkpass file for the User
     */
    //@RequestMapping(method = RequestMethod.GET)

    private static final String ANDROID = "ANDROID";
    private static final String IOS = "IOS";
    private static final String MAC_OS_X = "MAC_OS_X";

    @Transactional
    @Funq
    public ResponseEntity getPasskit(WalletPassRequest walletPassRequest) {

        System.out.println(walletPassRequest.getUserAgent());
        UserAgent userAgent = UserAgent.parseUserAgentString(walletPassRequest.getUserAgent());
        OperatingSystem os = userAgent.getOperatingSystem();
        //System.out.println(os.toString());
        //System.out.println(os.getId());
        //System.out.println(os.getDeviceType());
        //System.out.println(os.getManufacturer());
        //System.out.println(os.getName());
        //System.out.println(os.getGroup());
        if (os.getGroup().toString().equals(ANDROID)) {
            //TODO: Android Process
        } else if (os.getGroup().toString().equals(IOS) || os.getGroup().toString().equals(MAC_OS_X)) {
            System.out.println("etin: "+walletPassRequest.getETin());
            byte[] pkpassFile = walletPassService.createPasskit(walletPassRequest.getETin());

            // Prepare response to start download

            //ResponseBuilder response = Response.ok(pkpassFile);
            //response.header("Content-Disposition","inline; filename=\"" + walletPassService.getFileName(walletPassRequest.getETin()) +"\"");

            //HttpServletResponse response;
            //response.setStatus(200);
            //response.setContentLength(pkpassFile.length);
            //response.setContentType(PASSKIT_CONTENT_TYPE);
            //response.setHeader("Content-Disposition","inline; filename=\"" + walletPassService.getFileName(walletPassRequest.getETin()) +"\"");

            //ByteArrayInputStream bais = new ByteArrayInputStream(pkpassFile);
            try {
                //IOUtils.copy(bais, response.getOutputStream());
                //response.flushBuffer();
                //return response.build();
                HttpHeaders header = new HttpHeaders();
                header.setContentType(MediaType.valueOf(PASSKIT_CONTENT_TYPE));
                header.setContentLength(pkpassFile.length);
                header.setContentDispositionFormData("inline", walletPassService.getFileName(walletPassRequest.getETin()));
                return new ResponseEntity(pkpassFile
                        , header
                        , HttpStatus.OK);
            } catch (Exception e) {
                // log the exception.
                e.printStackTrace();
            }

        } else {
            ResponseBuilder response = Response.noContent();
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
            //TODO: do something here
        }

        return null;
    }
}
