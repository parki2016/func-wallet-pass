package au.com.parki.walletpass.funqy.service;

import au.com.parki.walletpass.funqy.dao.WalletPassDao;
import au.com.parki.walletpass.funqy.dto.PassDetails;
import au.com.parki.walletpass.funqy.model.TicketDetails;
import de.brendamour.jpasskit.PKBarcode;
import de.brendamour.jpasskit.PKField;
import de.brendamour.jpasskit.PKPass;
import de.brendamour.jpasskit.enums.PKBarcodeFormat;
import de.brendamour.jpasskit.passes.PKGenericPass;
import de.brendamour.jpasskit.signing.*;
import io.quarkus.runtime.Startup;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;

@Startup
@Singleton
public class WalletPassService {

    /**
     * Create a .pkpass file for User
     *
     * @param user the user
     * @return byte array representing the zipped archive of apple passkit
     */
    @Inject
    WalletPassDao walletPassDao;

    private static final String ETIN_FORMAT = "ETIN%s";

    public byte[] createPasskit(String eTin) {

        TicketDetails ticketDetails = walletPassDao.getTicketDetails(eTin);
        PassDetails passDetails = new PassDetails();//TODO: Edit properties
        //String delim = " ";

        PKPass pass = new PKPass();

        PKGenericPass gp = new PKGenericPass();

        // name, label, value
        PKField headerField = new PKField(passDetails.getHeaderFieldName(), passDetails.getHeaderHeader(), ticketDetails.getZoneName());
        PKField primaryField = new PKField(passDetails.getPrimaryFieldName(), passDetails.getPrimaryHeader(), ticketDetails.getRegistration());
        PKField secondaryField = new PKField(passDetails.getSecondaryFieldName(), passDetails.getSecondaryHeader(), ticketDetails.getStartDate().toString());
        PKField auxiliaryField = new PKField(passDetails.getAuxiliaryFieldName(), passDetails.getAuxiliaryHeader(), ticketDetails.getEndDate().toString());
        gp.addHeaderField(headerField);
        gp.addPrimaryField(primaryField);
        gp.addSecondaryField(secondaryField);
        gp.addAuxiliaryField(auxiliaryField);

        PKBarcode barcode = new PKBarcode();
        barcode.setFormat(PKBarcodeFormat.PKBarcodeFormatQR);
        barcode.setMessage(String.format(ETIN_FORMAT, eTin));
        barcode.setMessageEncoding(Charset.forName("utf-8"));
        List<PKBarcode> barcodeList = new ArrayList<PKBarcode>();
        barcodeList.add(barcode);

        pass.setBackgroundColor(passDetails.getBackgroundColor());
        pass.setFormatVersion(passDetails.getVersion());
        pass.setPassTypeIdentifier(passDetails.getPassTypeIdentifier());
        pass.setSerialNumber(ticketDetails.getEtin());
        pass.setTeamIdentifier(passDetails.getTeamID());
        pass.setOrganizationName(passDetails.getOrgName());
        pass.setDescription(passDetails.getDescription());
        pass.setForegroundColor(passDetails.getForegroundColor());
        pass.setGeneric(gp);
        pass.setBarcodes(barcodeList);

        PKPassTemplateInMemory pkPassTemplateInMemory = new PKPassTemplateInMemory();
        PKSigningInformation pkSigningInformation;
        try {
            System.out.println("trying...");
            ClassLoader classLoader =   Thread.currentThread().getContextClassLoader();
            InputStream logoFile =   classLoader.getResourceAsStream(passDetails.getLogoIconFileName());
            InputStream iconFile = classLoader.getResourceAsStream(passDetails.getLogoIconFileName());
            URL url = new URL(ticketDetails.getCarparkImage());

            pkPassTemplateInMemory.addFile(PKPassTemplateInMemory.PK_ICON, logoFile);
            pkPassTemplateInMemory.addFile(PKPassTemplateInMemory.PK_ICON_RETINA, logoFile);
            pkPassTemplateInMemory.addFile(PKPassTemplateInMemory.PK_LOGO, logoFile);
            pkPassTemplateInMemory.addFile(PKPassTemplateInMemory.PK_LOGO_RETINA, logoFile);
            pkPassTemplateInMemory.addFile(PKPassTemplateInMemory.PK_THUMBNAIL, url);
            pkPassTemplateInMemory.addFile(PKPassTemplateInMemory.PK_THUMBNAIL_RETINA, url);

            pkSigningInformation = new PKSigningInformationUtil()
                    .loadSigningInformationFromPKCS12AndIntermediateCertificate(
                            classLoader.getResourceAsStream(passDetails.getPassCertificateFileName()), passDetails.getPassword(),
                            classLoader.getResourceAsStream(passDetails.getDeveloperCertificateFileName()));

            PKFileBasedSigningUtil pkSigningUtil = new PKFileBasedSigningUtil();
            byte[] signedAndZippedPkPassArchive = pkSigningUtil.createSignedAndZippedPkPassArchive(pass,
                    pkPassTemplateInMemory, pkSigningInformation);

            return signedAndZippedPkPassArchive;
        } catch (CertificateException e1) {
            e1.printStackTrace();
            // log the exception.
        } catch (IOException e1) {
            e1.printStackTrace();
            // log the exception.
        } catch (PKSigningException e) {
            e.printStackTrace();
            // log the exception.
        }
        System.out.println("return will be null");
        return null;
    }

    /**
     * Returns the name of the pkpass file
     *
     * @return filename for pkpass file
     */
    public String getFileName(String eTin) {
        return "etin_" + eTin + ".pkpass";
    }
}
