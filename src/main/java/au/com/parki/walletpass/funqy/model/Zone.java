package au.com.parki.walletpass.funqy.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "zone")
public class Zone implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "zone_name")
    private String zoneName;

    @Column(name = "carpark_image")
    private String carparkImage;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public String getCarparkImage() {
        return carparkImage;
    }

    public void setCarparkImage(String carparkImage) {
        this.carparkImage = carparkImage;
    }
}
