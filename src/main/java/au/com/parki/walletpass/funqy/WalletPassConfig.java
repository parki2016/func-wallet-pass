package au.com.parki.walletpass.funqy;

import au.com.parki.ParkiTokenUtil;

import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;

@Dependent
public class WalletPassConfig {

    @Produces
    public ParkiTokenUtil parkiTokenUtil() {
        return new ParkiTokenUtil();
    }
}
