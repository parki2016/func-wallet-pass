package au.com.parki.walletpass.funqy.dto;

public class PassDetails {

    /** The foreground color of pass */
    private final String foregroundColor = "rgb(255,255,255)";

    /** The background color of pass */
    private final String backgroundColor = "rgb(207,18,45)";

    /** The pass type identifier, taken from the certificate */
    private final String passTypeIdentifier = "pass.au.com.cdsw.parki.app.generic";

    /** The team ID, taken from the certificate */
    private final String teamID = "SW76QFL975";

    /** version */
    private final int version = 1;

    /** The pass description */
    private final String description = "Example card";

    /** The pass type identifier, taken from the certificate */
    private final String logoIconFileName = "ab_icon_logo.png";

    /** The name of primary field of the pass */
    private final String headerFieldName = "zone";

    /** The name of primary header of the pass */
    private final String headerHeader = "Zone";

    /** The name of primary field of the pass */
    private final String primaryFieldName = "registration";

    /** The name of primary header of the pass */
    private final String primaryHeader = "Registration";

    /** The name of secondary field of the pass */
    private final String secondaryFieldName = "startdate";

    /** The name of secondary header of the pass */
    private final String secondaryHeader = "Entry Date and Time";

    /** The name of secondary field of the pass */
    private final String auxiliaryFieldName = "enddate";

    /** The name of secondary header of the pass */
    private final String auxiliaryHeader = "Exit Date and Time";

    /** The name of organization issuing the pass */
    private final String orgName = "CDS Worldwide";

    /** The Pass certificate path */
    private final String passCertificateFileName = "PassTypeIdCertificates.p12";

    /** The password for pass certificate */
    private final String password = "";

    /** The Apple developer certificate path */
    private final String developerCertificateFileName = "development.cer";


    public String getForegroundColor() {
        return foregroundColor;
    }
    public String getBackgroundColor() { return backgroundColor; }
    public String getPassTypeIdentifier() { return passTypeIdentifier; }
    public String getTeamID() { return teamID; }
    public int getVersion() { return version; }
    public String getDescription() { return description; }
    public String getLogoIconFileName() { return logoIconFileName; }
    public String getHeaderFieldName() { return headerFieldName; }
    public String getHeaderHeader() { return headerHeader; }
    public String getPrimaryFieldName() { return primaryFieldName; }
    public String getPrimaryHeader() { return primaryHeader; }
    public String getSecondaryFieldName() { return secondaryFieldName; }
    public String getSecondaryHeader() { return secondaryHeader; }
    public String getAuxiliaryFieldName() { return auxiliaryFieldName; }
    public String getAuxiliaryHeader() { return auxiliaryHeader; }
    public String getOrgName() { return orgName; }
    public String getPassCertificateFileName() { return passCertificateFileName; }
    public String getPassword() { return password; }
    public String getDeveloperCertificateFileName() { return developerCertificateFileName; }
}
